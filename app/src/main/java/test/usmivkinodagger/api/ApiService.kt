package test.usmivkinodagger.api

import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Streaming
import retrofit2.http.Url
import rx.Observable


interface ApiService {
    @Streaming
    @GET("{fileUrl}")
    fun downloadFileByUrlRx(@Url fileUrl: String): Observable<Response<ResponseBody>>

}