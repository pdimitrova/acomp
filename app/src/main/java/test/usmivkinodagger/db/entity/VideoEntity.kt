package test.usmivkinodagger.db.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "Video")
class VideoEntity(@PrimaryKey
                  val id: Int,
                  val name: String,
                  val description: String,
                  val isAvailable: Boolean,
                  val address: String,
                  val location: String?) {
    //   constructor() : this(1, "", "", false, "") {}

}
