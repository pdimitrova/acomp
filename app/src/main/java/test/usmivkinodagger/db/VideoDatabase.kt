package test.usmivkinodagger.db

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import android.support.annotation.VisibleForTesting
import test.usmivkinodagger.db.dao.VideoDao
import test.usmivkinodagger.db.entity.VideoEntity
import test.usmivkinodagger.util.AppExecutors

@Database(entities = [(VideoEntity::class)], version = 1)
abstract class VideoDatabase : RoomDatabase() {
    abstract fun videoDao(): VideoDao
    val isDatabaseCreated = MutableLiveData<Boolean>()
    private fun updateDatabaseCreated(context: Context) {
        if (context.getDatabasePath(DATABASE_NAME).exists()) {
            setDBCreated()
        }
    }

    companion object {
        @VisibleForTesting
        private val DATABASE_NAME = "basic-sample-db"
        @Volatile
        private var INSTANCE: VideoDatabase? = null

        fun getInstance(context: Context, appExecutors: AppExecutors): VideoDatabase {
            if (INSTANCE == null) {
                synchronized(VideoDatabase::class) {
                    if (INSTANCE == null) {
                        INSTANCE = buildDatabase(context, appExecutors)
                        INSTANCE!!.setDBCreated()
                    }
                }
            }
            return this.INSTANCE!!

        }


        private fun buildDatabase(context: Context, executors: AppExecutors) =
                Room.databaseBuilder(context.applicationContext
                        , VideoDatabase::class.java
                        , DATABASE_NAME).addCallback(object : RoomDatabase.Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        executors.diskIO().execute({
                            val videoDB = getInstance(context = context, appExecutors = executors)
                            val videos = DataGenerator.generateVideos()
                            insertData(videoDB, videos)
                            videoDB.setDBCreated()
                        })
                    }
                }).build()

        private fun insertData(database: VideoDatabase, videos: List<VideoEntity>) {
            database.runInTransaction({
                database.videoDao().insertAll(videos)
            })
        }


    }

    internal fun setDBCreated() {
        isDatabaseCreated.postValue(true)
    }

    fun getAllVideos(): LiveData<List<VideoEntity>> {
        return videoDao().getAllVideos()

    }
}
