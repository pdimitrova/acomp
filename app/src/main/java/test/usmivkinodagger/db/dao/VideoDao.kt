package test.usmivkinodagger.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import test.usmivkinodagger.db.entity.VideoEntity

@Dao
interface VideoDao {

    @Query("SELECT * FROM Video WHERE id = :id")
    @Transaction
    fun getVideoById(id: Int): LiveData<VideoEntity>

    @Query("SELECT * FROM Video")
    fun getAllVideos(): LiveData<List<VideoEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVideo(video: VideoEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(videos: List<VideoEntity>)

    @Query("DELETE FROM Video WHERE id=:id")
    fun deleteVideo(id: String)


    @Update(onConflict = OnConflictStrategy.ABORT)
    fun updateVideo(video: VideoEntity)
}