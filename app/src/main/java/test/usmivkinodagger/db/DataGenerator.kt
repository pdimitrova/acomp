package test.usmivkinodagger.db

import test.usmivkinodagger.db.entity.VideoEntity
import java.util.*

class DataGenerator {


    companion object {
        private val videoAddress = "http://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4"

        fun generateVideos(): List<VideoEntity> {

            val videos = ArrayList<VideoEntity>()
            // val videoEntity = VideoEntity()

            for (i in 0..9) {
                val videoEntity = VideoEntity(i, "sample video$i", "sample description text for video number$i", false, videoAddress, null)
                videos.add(videoEntity)
            }
            return videos
        }
    }
}
