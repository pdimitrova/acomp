package test.usmivkinodagger.viewmodel

import android.app.Application
import android.arch.lifecycle.*
import test.usmivkinodagger.BaseApp
import test.usmivkinodagger.db.entity.VideoEntity

class VideoPlayerViewModel(application: Application) : AndroidViewModel(application) {
    private var videoEntityId = MutableLiveData<Int>()
    private var medLiveData = MediatorLiveData<VideoEntity>()
    private var observableVideo = MediatorLiveData<VideoEntity>()
    val videoData: LiveData<VideoEntity> get() = medLiveData
    var mLocalLiveData: LiveData<VideoEntity>


    init {
        mLocalLiveData = Transformations.switchMap(this.videoEntityId) { it -> getApplication<BaseApp>().repository.loadVideo(it) }
    }

    fun setVideoId(vId: Int) {
        this.videoEntityId.postValue(vId)


    }


}

