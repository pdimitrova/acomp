package test.usmivkinodagger.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import okhttp3.ResponseBody
import okio.Okio
import retrofit2.Response
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import test.usmivkinodagger.BaseApp
import test.usmivkinodagger.db.entity.VideoEntity
import java.io.File


class VideoListViewModel(application: Application) : AndroidViewModel(application) {
    private var observableVideos: MediatorLiveData<List<VideoEntity>>
    val videoData: LiveData<List<VideoEntity>> get() = observableVideos
    val downloadVideoResult = MutableLiveData<Boolean>()


    init {
        val videos = (application as BaseApp).repository.videos
        observableVideos = MediatorLiveData()
        observableVideos.addSource(videos, { observableVideos.value = (it) })
    }

    fun downloadVideo(videoEntity: VideoEntity) {
        (getApplication<BaseApp>()).apiService.downloadFileByUrlRx(videoEntity.location!!)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ saveFile(it, videoEntity) })

    }

    private fun saveFile(response: Response<ResponseBody>, videoEntity: VideoEntity) {
        val fileName = videoEntity.name
        val file = File(getApplication<BaseApp>().filesDir, fileName)
        val bufferedSink = Okio.buffer(Okio.sink(file))
        bufferedSink.writeAll(response.body()!!.source())
        bufferedSink.close()

        downloadVideoResult.postValue(response.isSuccessful)
    } 

    private fun updateEntity() {
      TODO("send to repo to be updated")
    }

//    private fun processResponse(): Func1<Response<ResponseBody>, Any> {
//        return Func1<Response<ResponseBody>, Any> { responseBodyResponse -> saveToDiskRx(responseBodyResponse) }
//    }
//
//    private fun saveToDiskRx(response: Response<ResponseBody>): Observable<File> {
//        return Observable.create(object : Observable.OnSubscribe<File> {
//            override fun call(subscriber: Subscriber<in File>) {
//                try {
//                    val header = response.headers().get("Content-Disposition")
//                    val filename = header!!.replace("attachment; filename=", "")
//
//                    File("/data/data/" + getPackageName() + "/games").mkdir()
//                    val destinationFile = File("/data/data/" + getPackageName() + "/games/" + filename)
//
//                    val bufferedSink = Okio.buffer(Okio.sink(destinationFile))
//                    bufferedSink.writeAll(response.body()!!.source())
//                    bufferedSink.close()
//
//                    subscriber.onNext(destinationFile)
//                    subscriber.onCompleted()
//                } catch (e: IOException) {
//                    e.printStackTrace()
//                    subscriber.onError(e)
//                }
//
//            }
//        },Emitter.BackpressureMode.LATEST)
//    }


}
//        mObservableProducts.addSource(products, Observer<List<ProductEntity>> { mObservableProducts.setValue(it) })
