package test.usmivkinodagger

import android.app.Application
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import test.usmivkinodagger.api.ApiService
import test.usmivkinodagger.db.VideoDatabase
import test.usmivkinodagger.repo.VideoRepository
import test.usmivkinodagger.util.AppExecutors
import java.net.ProxySelector
import java.util.concurrent.TimeUnit

class BaseApp : Application() {

    private val appExecutors by lazy { AppExecutors() }
    private val database: VideoDatabase
        get() = appExecutors.let { VideoDatabase.getInstance(this, it) }
    val repository: VideoRepository get() = VideoRepository.getInstance(database = database)
    val apiService by lazy {
        createApiService()
    }

    private fun getHttpClient(): OkHttpClient {


        return OkHttpClient.Builder()
                .proxySelector(ProxySelector.getDefault())
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build()
    }

    private fun createApiService(): ApiService {
        val retrofit = Retrofit.Builder().client(getHttpClient()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
        val apiService = retrofit.create(ApiService::class.java)
        return apiService
    }

}
