package test.usmivkinodagger.ui

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearLayoutManager.VERTICAL
import android.widget.Toast

import test.usmivkinodagger.R
import test.usmivkinodagger.adapter.VideoAdapter
import test.usmivkinodagger.databinding.ActivityVideoListBinding
import test.usmivkinodagger.db.entity.VideoEntity
import test.usmivkinodagger.ui.uicallback.DownloadVideoCallback
import test.usmivkinodagger.ui.uicallback.WatchVideoCallback
import test.usmivkinodagger.viewmodel.VideoListViewModel

class VideoListActivity : AppCompatActivity() {

    private lateinit var videoListBinding: ActivityVideoListBinding
    private lateinit var videosAdapter: VideoAdapter

    private val watchVideoCallback = object : WatchVideoCallback {
        override fun onClick(video: VideoEntity) {

            if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                watchVideo(video)
            }
        }
    }


    private val downloadVideoCallback = object : DownloadVideoCallback {
        override fun onClick(video: VideoEntity) {

            if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                downloadVideo(video)
            }
        }
    }

    private fun downloadVideo(videoEntity: VideoEntity) {

        Toast.makeText(this, "Download video ${videoEntity.name}", Toast.LENGTH_SHORT).show()

    }


    private fun watchVideo(videoEntity: VideoEntity) {
        Toast.makeText(this, "Watch video ${videoEntity.name}", Toast.LENGTH_SHORT).show()
        VideoPlayerActivity.start(this, videoEntity.id)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        videoListBinding = DataBindingUtil.setContentView(this, R.layout.activity_video_list)
        val viewModel: VideoListViewModel = ViewModelProviders.of(this).get(VideoListViewModel::class.java)
        videosAdapter = VideoAdapter(watchVideoCallback, downloadVideoCallback)
        videoListBinding.videoListRecyclerView.adapter = videosAdapter
        videoListBinding.videoListRecyclerView.layoutManager = LinearLayoutManager(this, VERTICAL, false)
        subscribeUi(viewModel)

    }

    private fun subscribeUi(viewModel: VideoListViewModel) {
        viewModel.videoData.observe(this, Observer { myVideos ->
            if (myVideos != null) {
                videoListBinding.isLoading = false
                videosAdapter.setDataSet(myVideos)
            } else {
                videoListBinding.isLoading = true
            }
            videoListBinding.executePendingBindings()
        })


    }
}
