package test.usmivkinodagger.ui.uicallback

import test.usmivkinodagger.db.entity.VideoEntity

interface DownloadVideoCallback {
    fun onClick(video: VideoEntity)
}