package test.usmivkinodagger.ui.uicallback

import test.usmivkinodagger.db.entity.VideoEntity

interface WatchVideoCallback {
    fun onClick(video: VideoEntity)
}