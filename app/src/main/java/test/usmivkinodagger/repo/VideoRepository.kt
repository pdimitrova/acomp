package test.usmivkinodagger.repo

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import test.usmivkinodagger.db.VideoDatabase
import test.usmivkinodagger.db.entity.VideoEntity

class VideoRepository private constructor(private val mDatabase: VideoDatabase) {
    private val mObservableVideos: MediatorLiveData<List<VideoEntity>> = MediatorLiveData()

    /**
     * Get the list of videos from the database and get notified when the data changes.
     */
    val videos: LiveData<List<VideoEntity>>
        get() {
            return mObservableVideos
        }

    init {

        mObservableVideos.addSource(mDatabase.videoDao().getAllVideos()
        ) { videoEntities ->
            if (mDatabase.isDatabaseCreated.value != null) {
                mObservableVideos.postValue(videoEntities)
            }
        }
    }

    private val chosenVideoId = MutableLiveData<Int>()


    private val chosenVideo = Transformations.switchMap(chosenVideoId) { it ->
        mDatabase.videoDao().getVideoById(it)
    }

    fun loadVideo(videoId: Int): LiveData<VideoEntity> {
        chosenVideoId.value = (videoId)
        return chosenVideo
    }

    fun updateVideo(videoEntity: VideoEntity): LiveData<VideoEntity> {
        TODO("research vao update and see how this can be handled")
    }

    companion object {
        private var sInstance: VideoRepository? = null

        fun getInstance(database: VideoDatabase): VideoRepository {
            if (sInstance == null) {
                synchronized(VideoRepository::class.java) {
                    if (sInstance == null) {
                        sInstance = VideoRepository(database)
                    }
                }
            }
            return this.sInstance!!
        }
    }

}
