package test.usmivkinodagger.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import test.usmivkinodagger.R
import test.usmivkinodagger.databinding.VideoItemBinding
import test.usmivkinodagger.db.entity.VideoEntity
import test.usmivkinodagger.ui.uicallback.DownloadVideoCallback
import test.usmivkinodagger.ui.uicallback.WatchVideoCallback

class VideoAdapter(private var watchVideoCallback: WatchVideoCallback?, private var downloadVideoCallback: DownloadVideoCallback?) : RecyclerView.Adapter<VideoAdapter.VideoViewHolder>() {
    private var videoList: List<VideoEntity>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewHolder {
        val binding = DataBindingUtil.inflate<VideoItemBinding>(LayoutInflater.from(parent.context), R.layout.video_item, parent, false)
        return VideoViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (videoList == null) 0 else videoList!!.size
    }

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        holder.binding.video = videoList?.get(position)
        holder.binding.downloadVideoCallback = downloadVideoCallback
        holder.binding.watchVideoCallback = watchVideoCallback

    }

    fun setDataSet(newList: List<VideoEntity>) {
        this.videoList = newList
        notifyItemRangeChanged(0, newList.size)
    }

    class VideoViewHolder(val binding: VideoItemBinding) : RecyclerView.ViewHolder(binding.root)
}

